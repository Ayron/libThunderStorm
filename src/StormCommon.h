/*****************************************************************************/
/* SCommon.h                              Copyright (c) Ladislav Zezula 2003 */
/*---------------------------------------------------------------------------*/
/* Common functions for encryption/decryption from Storm.dll. Included by    */
/* SFile*** functions, do not include and do not use this file directly      */
/*---------------------------------------------------------------------------*/
/*   Date    Ver   Who  Comment                                              */
/* --------  ----  ---  -------                                              */
/* 24.03.03  1.00  Lad  The first version of SFileCommon.h                   */
/* 12.06.04  1.00  Lad  Renamed to SCommon.h                                 */
/* 06.09.10  1.00  Lad  Renamed to StormCommon.h                             */
/* 10.03.15  1.00  Ayr  Ported to plain c                                    */
/*****************************************************************************/

#ifndef _STORMCOMMON_H
#define _STORMCOMMON_H

#include "config.h"
#include "thunderStorm.h"

/* Definitions */
#define EXPORT_SYMBOL __attribute__ ((visibility ("default")))

/*-----------------------------------------------------------------------------
 * Compression support
 */

/* Include functions from Pkware Data Compression Library */
#include "pklib/pklib.h"

/* Include functions from Huffmann compression */
#include "huffman/huff.h"

/* Include functions from IMA ADPCM compression */
#include "adpcm/adpcm.h"

/* Include functions from SPARSE compression */
#include "sparse/sparse.h"

/* Include functions from LZMA compression */
#include "lzma/C/LzmaEnc.h"
#include "lzma/C/LzmaDec.h"

/* Include functions from zlib */
#ifndef __SYS_ZLIB
  #include "zlib/zlib.h"
#else
  #include <zlib.h>
#endif

/* Include functions from bzlib */
#ifndef __SYS_BZLIB
  #include "bzip2/bzlib.h"
#else
  #include <bzlib.h>
#endif

/*-----------------------------------------------------------------------------
 * Cryptography support
 */

/* Headers from LibTomCrypt */
#include "libtomcrypt/src/headers/tomcrypt.h"

/* For HashStringJenkins */
#include "jenkins/lookup.h"

/* Anubis cipher */
#include "anubis/anubis.h"

/* Serpent cipher */
#include "serpent/serpent.h"

/*-----------------------------------------------------------------------------
 * StormLib private defines
 */

#define ID_MPQ_FILE            0x46494c45     /* Used internally for checking TMPQFile ('FILE') */

/* Prevent problems with CRT "min" and "max" functions, */
/* as they are not defined on all platforms */
#define STORMLIB_MIN(a, b) ((a < b) ? a : b)
#define STORMLIB_MAX(a, b) ((a > b) ? a : b)
#define STORMLIB_UNUSED(p) ((void)(p))

/* Macro for building 64-bit file offset from two 32-bit */
#define MAKE_OFFSET64(hi, lo)      (((uint64_t)hi << 32) | (uint64_t)lo)

/*-----------------------------------------------------------------------------
 * Structures related to MPQ format
 *
 * Note: All structures in this header file are supposed to remain private
 * to StormLib. The structures may (and will) change over time, as the MPQ
 * file format evolves. Programmers directly using these structures need to
 * be aware of this. And the last, but not least, NEVER do any modifications
 * to those structures directly, always use SFile* functions.
 */

#define MPQ_HEADER_SIZE_V1    0x20
#define MPQ_HEADER_SIZE_V2    0x2C
#define MPQ_HEADER_SIZE_V3    0x44
#define MPQ_HEADER_SIZE_V4    0xD0
#define MPQ_HEADER_WORDS      (MPQ_HEADER_SIZE_V4 / 0x04)

typedef struct _TMPQUserData
{
    /* The ID_MPQ_USERDATA ('MPQ\x1B') signature */
    uint32_t dwID;

    /* Maximum size of the user data */
    uint32_t cbUserDataSize;

    /* Offset of the MPQ header, relative to the begin of this header */
    uint32_t dwHeaderOffs;

    /* Appears to be size of user data header (Starcraft II maps) */
    uint32_t cbUserDataHeader;
} TMPQUserData;

/* MPQ file header
 *
 * We have to make sure that the header is packed OK.
 * Reason: A 64-bit integer at the beginning of 3.0 part,
 * which is offset 0x2C
 */
#pragma pack(push, 1)
typedef struct _TMPQHeader
{
    /* The ID_MPQ ('MPQ\x1A') signature */
    uint32_t dwID;

    /* Size of the archive header */
    uint32_t dwHeaderSize;

    /* 32-bit size of MPQ archive
     * This field is deprecated in the Burning Crusade MoPaQ format, and the size of the archive
     * is calculated as the size from the beginning of the archive to the end of the hash table,
     * block table, or hi-block table (whichever is largest).
     */
    uint32_t dwArchiveSize;

    /* 0 = Format 1 (up to The Burning Crusade)
     * 1 = Format 2 (The Burning Crusade and newer)
     * 2 = Format 3 (WoW - Cataclysm beta or newer)
     * 3 = Format 4 (WoW - Cataclysm beta or newer)
     */
    uint16_t wFormatVersion;

    /* Power of two exponent specifying the number of 512-byte disk sectors in each file sector */
    /* in the archive. The size of each file sector in the archive is 512 * 2 ^ wSectorSize. */
    uint16_t wSectorSize;

    /* Offset to the beginning of the hash table, relative to the beginning of the archive. */
    uint32_t dwHashTablePos;
    
    /* Offset to the beginning of the block table, relative to the beginning of the archive. */
    uint32_t dwBlockTablePos;
    
    /* Number of entries in the hash table. Must be a power of two, and must be less than 2^16 for */
    /* the original MoPaQ format, or less than 2^20 for the Burning Crusade format. */
    uint32_t dwHashTableSize;
    
    /* Number of entries in the block table */
    uint32_t dwBlockTableSize;

    /*-- MPQ HEADER v 2 -------------------------------------------*/

    /* Offset to the beginning of array of 16-bit high parts of file offsets. */
    uint64_t HiBlockTablePos64;

    /* High 16 bits of the hash table offset for large archives. */
    uint16_t wHashTablePosHi;

    /* High 16 bits of the block table offset for large archives. */
    uint16_t wBlockTablePosHi;

    /*-- MPQ HEADER v 3 ------------------------------------------- */

    /* 64-bit version of the archive size */
    uint64_t ArchiveSize64;

    /* 64-bit position of the BET table */
    uint64_t BetTablePos64;

    /* 64-bit position of the HET table */
    uint64_t HetTablePos64;

    /*-- MPQ HEADER v 4 -------------------------------------------*/

    /* Compressed size of the hash table */
    uint64_t HashTableSize64;

    /* Compressed size of the block table */
    uint64_t BlockTableSize64;

    /* Compressed size of the hi-block table */
    uint64_t HiBlockTableSize64;

    /* Compressed size of the HET block */
    uint64_t HetTableSize64;

    /* Compressed size of the BET block */
    uint64_t BetTableSize64;

    /* Size of raw data chunk to calculate MD5. */
    /* MD5 of each data chunk follows the raw file data. */
    uint32_t dwRawChunkSize;                                 

    /* MD5 of MPQ tables */
    unsigned char MD5_BlockTable[MD5_DIGEST_SIZE];      /* MD5 of the block table before decryption */
    unsigned char MD5_HashTable[MD5_DIGEST_SIZE];       /* MD5 of the hash table before decryption */
    unsigned char MD5_HiBlockTable[MD5_DIGEST_SIZE];    /* MD5 of the hi-block table */
    unsigned char MD5_BetTable[MD5_DIGEST_SIZE];        /* MD5 of the BET table before decryption */
    unsigned char MD5_HetTable[MD5_DIGEST_SIZE];        /* MD5 of the HET table before decryption */
    unsigned char MD5_MpqHeader[MD5_DIGEST_SIZE];       /* MD5 of the MPQ header from signature to (including) MD5_HetTable */
} TMPQHeader;

/* Hash table entry. All files in the archive are searched by their hashes. */
typedef struct _TMPQHash
{
    /* The hash of the file path, using method A. */
    uint32_t dwName1;
    
    /* The hash of the file path, using method B. */
    uint32_t dwName2;

#ifdef PLATFORM_LITTLE_ENDIAN

    /* The language of the file. This is a Windows LANGID data type, and uses the same values. */
    /* 0 indicates the default language (American English), or that the file is language-neutral. */
    uint16_t lcLocale;

    /* The platform the file is used for. 0 indicates the default platform. */
    /* No other values have been observed. */
    /* Note: wPlatform is actually just uint8_t, but since it has never been used, we don't care. */
    uint16_t wPlatform;

#else

    uint16_t wPlatform;
    uint16_t lcLocale;

#endif

    /* If the hash table entry is valid, this is the index into the block table of the file.
     * Otherwise, one of the following two values:
     *  - FFFFFFFFh: Hash table entry is empty, and has always been empty.
     *               Terminates searches for a given file.
     *  - FFFFFFFEh: Hash table entry is empty, but was valid at some point (a deleted file).
     *               Does not terminate searches for a given file.
     */
    uint32_t dwBlockIndex;
} TMPQHash;
#pragma pack(pop)

/* File description block contains informations about the file */
typedef struct _TMPQBlock
{
    /* Offset of the beginning of the file, relative to the beginning of the archive. */
    uint32_t dwFilePos;
    
    /* Compressed file size */
    uint32_t dwCSize;
    
    /* Only valid if the block is a file; otherwise meaningless, and should be 0. */
    /* If the file is compressed, this is the size of the uncompressed file data. */
    uint32_t dwFSize;                      
    
    /* Flags for the file. See MPQ_FILE_XXXX constants */
    uint32_t dwFlags;                      
} TMPQBlock;

/* Patch file information, preceding the sector offset table */
typedef struct _TPatchInfo
{
    uint32_t dwLength;                             /* Length of patch info header, in bytes */
    uint32_t dwFlags;                              /* Flags. 0x80000000 = MD5 (?) */
    uint32_t dwDataSize;                           /* Uncompressed size of the patch file */
    uint8_t  md5[0x10];                            /* MD5 of the entire patch file after decompression */

    /* Followed by the sector table (variable length) */
} TPatchInfo;

/* This is the combined file entry for maintaining file list in the MPQ. */
/* This structure is combined from block table, hi-block table, */
/* (attributes) file and from (listfile). */
typedef struct _TFileEntry
{
    uint64_t FileNameHash;                     /* Jenkins hash of the file name. Only used when the MPQ has BET table. */
    uint64_t ByteOffset;                       /* Position of the file content in the MPQ, relative to the MPQ header */
    uint64_t FileTime;                         /* FileTime from the (attributes) file. 0 if not present. */
    uint32_t dwFileSize;                       /* Decompressed size of the file */
    uint32_t dwCmpSize;                        /* Compressed size of the file (i.e., size of the file data in the MPQ) */
    uint32_t dwFlags;                          /* File flags (from block table) */
    uint32_t dwCrc32;                          /* CRC32 from (attributes) file. 0 if not present. */
    unsigned char md5[MD5_DIGEST_SIZE];         /* File MD5 from the (attributes) file. 0 if not present. */
    char * szFileName;                          /* File name. NULL if not known. */
} TFileEntry;

/* Common header for HET and BET tables */
typedef struct _TMPQExtHeader
{
    uint32_t dwSignature;                          /* 'HET\x1A' or 'BET\x1A' */
    uint32_t dwVersion;                            /* Version. Seems to be always 1 */
    uint32_t dwDataSize;                           /* Size of the contained table */

    /* Followed by the table header */
    /* Followed by the table data */

} TMPQExtHeader;

/* Structure for HET table header */
typedef struct _TMPQHetHeader
{
    TMPQExtHeader ExtHdr;

    uint32_t dwTableSize;                      /* Size of the entire HET table, including HET_TABLE_HEADER (in bytes) */
    uint32_t dwEntryCount;                     /* Number of occupied entries in the HET table */
    uint32_t dwTotalCount;                     /* Total number of entries in the HET table */
    uint32_t dwNameHashBitSize;                /* Size of the name hash entry (in bits) */
    uint32_t dwIndexSizeTotal;                 /* Total size of file index (in bits) */
    uint32_t dwIndexSizeExtra;                 /* Extra bits in the file index */
    uint32_t dwIndexSize;                      /* Effective size of the file index (in bits) */
    uint32_t dwIndexTableSize;                 /* Size of the block index subtable (in bytes) */

} TMPQHetHeader;

/* Structure for BET table header */
typedef struct _TMPQBetHeader
{
    TMPQExtHeader ExtHdr;

    uint32_t dwTableSize;                      /* Size of the entire BET table, including the header (in bytes) */
    uint32_t dwEntryCount;                     /* Number of entries in the BET table. Must match HET_TABLE_HEADER::dwEntryCount */
    uint32_t dwUnknown08;
    uint32_t dwTableEntrySize;                 /* Size of one table entry (in bits) */
    uint32_t dwBitIndex_FilePos;               /* Bit index of the file position (within the entry record) */
    uint32_t dwBitIndex_FileSize;              /* Bit index of the file size (within the entry record) */
    uint32_t dwBitIndex_CmpSize;               /* Bit index of the compressed size (within the entry record) */
    uint32_t dwBitIndex_FlagIndex;             /* Bit index of the flag index (within the entry record) */
    uint32_t dwBitIndex_Unknown;               /* Bit index of the ??? (within the entry record) */
    uint32_t dwBitCount_FilePos;               /* Bit size of file position (in the entry record) */
    uint32_t dwBitCount_FileSize;              /* Bit size of file size (in the entry record) */
    uint32_t dwBitCount_CmpSize;               /* Bit size of compressed file size (in the entry record) */
    uint32_t dwBitCount_FlagIndex;             /* Bit size of flags index (in the entry record) */
    uint32_t dwBitCount_Unknown;               /* Bit size of ??? (in the entry record) */
    uint32_t dwBitTotal_NameHash2;             /* Total bit size of the NameHash2 */
    uint32_t dwBitExtra_NameHash2;             /* Extra bits in the NameHash2 */
    uint32_t dwBitCount_NameHash2;             /* Effective size of NameHash2 (in bits) */
    uint32_t dwNameHashArraySize;              /* Size of NameHash2 table, in bytes */
    uint32_t dwFlagCount;                      /* Number of flags in the following array */

} TMPQBetHeader;

/* Structure for parsed HET table */
typedef struct _TMPQHetTable
{
    TBitArray * pBetIndexes;                    /* Bit array of FileIndex values */
    uint8_t *     pNameHashes;                     /* Array of NameHash1 values (NameHash1 = upper 8 bits of FileName hashe) */
    uint64_t  AndMask64;                       /* AND mask used for calculating file name hash */
    uint64_t  OrMask64;                        /* OR mask used for setting the highest bit of the file name hash */

    uint32_t      dwEntryCount;                    /* Number of occupied entries in the HET table */
    uint32_t      dwTotalCount;                    /* Number of entries in both NameHash and FileIndex table */
    uint32_t      dwNameHashBitSize;               /* Size of the name hash entry (in bits) */
    uint32_t      dwIndexSizeTotal;                /* Total size of one entry in pBetIndexes (in bits) */
    uint32_t      dwIndexSizeExtra;                /* Extra bits in the entry in pBetIndexes */
    uint32_t      dwIndexSize;                     /* Effective size of one entry in pBetIndexes (in bits) */
} TMPQHetTable;

/* Structure for parsed BET table */
typedef struct _TMPQBetTable
{
    TBitArray * pNameHashes;                    /* Array of NameHash2 entries (lower 24 bits of FileName hash) */
    TBitArray * pFileTable;                     /* Bit-based file table */
    uint32_t * pFileFlags;                         /* Array of file flags */

    uint32_t dwTableEntrySize;                     /* Size of one table entry, in bits */
    uint32_t dwBitIndex_FilePos;                   /* Bit index of the file position in the table entry */
    uint32_t dwBitIndex_FileSize;                  /* Bit index of the file size in the table entry */
    uint32_t dwBitIndex_CmpSize;                   /* Bit index of the compressed size in the table entry */
    uint32_t dwBitIndex_FlagIndex;                 /* Bit index of the flag index in the table entry */
    uint32_t dwBitIndex_Unknown;                   /* Bit index of ??? in the table entry */
    uint32_t dwBitCount_FilePos;                   /* Size of file offset (in bits) within table entry */
    uint32_t dwBitCount_FileSize;                  /* Size of file size (in bits) within table entry */
    uint32_t dwBitCount_CmpSize;                   /* Size of compressed file size (in bits) within table entry */
    uint32_t dwBitCount_FlagIndex;                 /* Size of flag index (in bits) within table entry */
    uint32_t dwBitCount_Unknown;                   /* Size of ??? (in bits) within table entry */
    uint32_t dwBitTotal_NameHash2;                 /* Total size of the NameHash2 */
    uint32_t dwBitExtra_NameHash2;                 /* Extra bits in the NameHash2 */
    uint32_t dwBitCount_NameHash2;                 /* Effective size of the NameHash2 */
    uint32_t dwEntryCount;                         /* Number of entries */
    uint32_t dwFlagCount;                          /* Number of file flags in pFileFlags */
} TMPQBetTable;

/* Structure for patch prefix */
typedef struct _TMPQNamePrefix
{
    size_t nLength;                             /* Length of this patch prefix. Can be 0 */
    char szPatchPrefix[1];                      /* Patch name prefix (variable length). If not empty, it always starts with backslash. */
} TMPQNamePrefix;

/* Structure for name cache */
typedef struct _TMPQNameCache
{
    uint32_t FirstNameOffset;                   /* Offset of the first name in the name list (in bytes) */
    uint32_t FreeSpaceOffset;                   /* Offset of the first free byte in the name cache (in bytes) */
    uint32_t TotalCacheSize;                    /* Size, in bytes, of the cache. Includes wildcard */
    uint32_t SearchOffset;                      /* Used by SListFileFindFirstFile */

    /* Followed by search mask (ASCIIZ, '\0' if none) */
    /* Followed by name cache (ANSI multistring) */

} TMPQNameCache;

/* Archive handle structure */
struct _TMPQArchive
{
    TFileStream  * pStream;                     /* Open stream for the MPQ */

    uint64_t      UserDataPos;                 /* Position of user data (relative to the begin of the file) */
    uint64_t      MpqPos;                      /* MPQ header offset (relative to the begin of the file) */
    uint64_t      FileSize;                    /* Size of the file at the moment of file open */

    struct _TMPQArchive * haPatch;              /* Pointer to patch archive, if any */
    struct _TMPQArchive * haBase;               /* Pointer to base ("previous version") archive, if any */
    TMPQNamePrefix * pPatchPrefix;              /* Patch prefix to precede names of patch files */

    TMPQUserData * pUserData;                   /* MPQ user data (NULL if not present in the file) */
    TMPQHeader   * pHeader;                     /* MPQ file header */
    TMPQHash     * pHashTable;                  /* Hash table */
    TMPQHetTable * pHetTable;                   /* HET table */
    TFileEntry   * pFileTable;                  /* File table */
    HASH_STRING    pfnHashString;               /* Hashing function that will convert the file name into hash */
    
    TMPQUserData   UserData;                    /* MPQ user data. Valid only when ID_MPQ_USERDATA has been found */
    uint32_t          HeaderData[MPQ_HEADER_WORDS];  /* Storage for MPQ header */

    uint32_t          dwHETBlockSize;
    uint32_t          dwBETBlockSize;
    uint32_t          dwMaxFileCount;              /* Maximum number of files in the MPQ. Also total size of the file table. */
    uint32_t          dwFileTableSize;             /* Current size of the file table, e.g. index of the entry past the last occupied one */
    uint32_t          dwReservedFiles;             /* Number of entries reserved for internal MPQ files (listfile, attributes) */
    uint32_t          dwSectorSize;                /* Default size of one file sector */
    uint32_t          dwFileFlags1;                /* Flags for (listfile) */
    uint32_t          dwFileFlags2;                /* Flags for (attributes) */
    uint32_t          dwFileFlags3;                /* Flags for (signature) */
    uint32_t          dwAttrFlags;                 /* Flags for the (attributes) file, see MPQ_ATTRIBUTE_XXX */
    uint32_t          dwFlags;                     /* See MPQ_FLAG_XXXXX */
    uint32_t          dwSubType;                   /* See MPQ_SUBTYPE_XXX */

    SFILE_ADDFILE_CALLBACK pfnAddFileCB;        /* Callback function for adding files */
    void         * pvAddFileUserData;           /* User data thats passed to the callback */

    SFILE_COMPACT_CALLBACK pfnCompactCB;        /* Callback function for compacting the archive */
    uint64_t      CompactBytesProcessed;       /* Amount of bytes that have been processed during a particular compact call */
    uint64_t      CompactTotalBytes;           /* Total amount of bytes to be compacted */
    void         * pvCompactUserData;           /* User data thats passed to the callback */
    anubisSchedule_t    keyScheduleAnubis;      /* Key schedule for anubis encryption */
    serpentSchedule_t   keyScheduleSerpent;     /* Key schedule for serpent encryption */
    rsa_key             keyRSA;                 /* RSA key for archive signing and verifying */
};

/* File handle structure */
struct _TMPQFile
{
    TFileStream  * pStream;                     /* File stream. Only used on local files */
    TMPQArchive  * ha;                          /* Archive handle */
    TMPQHash     * pHashEntry;                  /* Pointer to hash table entry, if the file was open using hash table */
    TFileEntry   * pFileEntry;                  /* File entry for the file */
    uint64_t      RawFilePos;                   /* Offset in MPQ archive (relative to file begin) */
    uint64_t      MpqFilePos;                   /* Offset in MPQ archive (relative to MPQ header) */
    uint32_t          dwHashIndex;              /* Hash table index (0xFFFFFFFF if not used) */
    uint32_t          dwFileKey;                /* Decryption key */
    uint32_t          dwFilePos;                /* Current file position */
    uint32_t          dwMagic;                  /* 'FILE' */

    struct _TMPQFile * hfPatch;                 /* Pointer to opened patch file */

    TPatchInfo   * pPatchInfo;                  /* Patch info block, preceding the sector table */
    uint32_t     * SectorOffsets;               /* Position of each file sector, relative to the begin of the file. Only for compressed files. */
    uint32_t     * SectorChksums;               /* Array of sector checksums (either ADLER32 or MD5) values for each file sector */
    uint8_t      * pbFileData;                  /* Data of the file (single unit files, patched files) */
    uint32_t       cbFileData;                  /* Size of file data */
    uint32_t          dwCompression0;              /* Compression that will be used on the first file sector */
    uint32_t          dwSectorCount;               /* Number of sectors in the file */
    uint32_t          dwPatchedFileSize;           /* Size of patched file. Used when saving patch file to the MPQ */
    uint32_t          dwDataSize;                  /* Size of data in the file (on patch files, this differs from file size in block table entry) */

    uint8_t *         pbFileSector;                /* Last loaded file sector. For single unit files, entire file content */
    uint32_t          dwSectorOffs;                /* File position of currently loaded file sector */
    uint32_t          dwSectorSize;                /* Size of the file sector. For single unit files, this is equal to the file size */

    unsigned char  hctx[HASH_STATE_SIZE];       /* Hash state for MD5. Used when saving file to MPQ */
    uint32_t          dwCrc32;                     /* CRC32 value, used when saving file to MPQ */

    int            nAddFileError;               /* Result of the "Add File" operations */

    int           bLoadedSectorCRCs;           /* If true, we already tried to load sector CRCs */
    int           bCheckSectorCRCs;            /* If true, then SFileReadFile will check sector CRCs when reading the file */
    int           bIsWriteHandle;              /* If true, this handle has been created by SFileCreateFile */
};

/*-----------------------------------------------------------------------------
 * MPQ signature information
 */

/* Size of each signature type */
#define MPQ_WEAK_SIGNATURE_SIZE                 64
#define MPQ_STRONG_SIGNATURE_SIZE              256 
#define MPQ_SECURE_SIGNATURE_SIZE              512
#define MPQ_STRONG_SIGNATURE_ID         0x5349474E      /* ID of the strong signature ("NGIS") */
#define MPQ_SIGNATURE_FILE_SIZE (MPQ_WEAK_SIGNATURE_SIZE + 8)
#define MPQ_SECURE_SIGNATURE_FILE_SIZE (MPQ_SECURE_SIGNATURE_SIZE + 8)

/* MPQ signature info */
typedef struct _MPQ_SIGNATURE_INFO
{
    uint64_t BeginMpqData;                     /* File offset where the hashing starts */
    uint64_t BeginExclude;                     /* Begin of the excluded area (used for (signature) file) */
    uint64_t EndExclude;                       /* End of the excluded area (used for (signature) file) */
    uint64_t EndMpqData;                       /* File offset where the hashing ends */
    uint64_t EndOfFile;                        /* Size of the entire file */
    uint8_t  Signature[MPQ_SECURE_SIGNATURE_SIZE + 0x10];
    uint32_t cbSignatureSize;                      /* Length of the signature */
    uint32_t SignatureTypes;                       /* See SIGNATURE_TYPE_XXX */

} MPQ_SIGNATURE_INFO, *PMPQ_SIGNATURE_INFO;

/*-----------------------------------------------------------------------------
 * Memory management
 *
 * We use our own macros for allocating/freeing memory. If you want
 * to redefine them, please keep the following rules:
 *
 *  - The memory allocation must return NULL if not enough memory
 *    (i.e not to throw exception)
 *  - The allocating function does not need to fill the allocated buffer with zeros
 *  - Memory freeing function doesn't have to test the pointer to NULL
 */

#define STORM_ALLOC(type, nitems) (type *)malloc((nitems) * sizeof(type))
#define STORM_REALLOC(type, ptr, nitems) (type *)realloc(ptr, ((nitems) * sizeof(type)))
#define STORM_FREE(ptr)           free(ptr)

/*-----------------------------------------------------------------------------
 * StormLib internal global variables
 */

extern uint32_t lcFileLocale;                       /* Preferred file locale */

/*-----------------------------------------------------------------------------
 * Conversion to uppercase/lowercase (and "/" to "\")
 */

extern unsigned char AsciiToLowerTable[256];
extern unsigned char AsciiToUpperTable[256];

/*-----------------------------------------------------------------------------
 * Encryption and decryption functions
 */

#define MPQ_HASH_TABLE_INDEX    0x000
#define MPQ_HASH_NAME_A         0x100
#define MPQ_HASH_NAME_B         0x200
#define MPQ_HASH_FILE_KEY       0x300
#define MPQ_HASH_KEY2_MIX       0x400

uint32_t HashString(const char * szFileName, uint32_t dwHashType);
uint32_t HashStringCS(const char * szFileName, uint32_t dwHashType);
uint32_t HashStringSlash(const char * szFileName, uint32_t dwHashType);
uint32_t HashStringLower(const char * szFileName, uint32_t dwHashType);

void  InitializeMpqCryptography();

uint32_t GetHashTableSizeForFileCount(uint32_t dwFileCount);

int IsPseudoFileName(const char * szFileName, uint32_t * pdwFileIndex);
uint64_t HashStringJenkins(const char * szFileName);
uint64_t HashStringJenkinsCS(const char * szFileName);

uint32_t GetDefaultSpecialFileFlags(uint32_t dwFileSize, uint16_t wFormatVersion);

void  EncryptMpqBlock(void * pvDataBlock, uint32_t dwLength, uint32_t dwKey);
void  DecryptMpqBlock(void * pvDataBlock, uint32_t dwLength, uint32_t dwKey);
void  EncryptMpqBlockAnubis(void * pvDataBlock, uint32_t dwLength, anubisSchedule_t * keySchedule);
void  DecryptMpqBlockAnubis(void * pvDataBlock, uint32_t dwLength, anubisSchedule_t * keySchedule);
void  EncryptMpqBlockSerpent(void * pvDataBlock, uint32_t dwLength, serpentSchedule_t * keySchedule);
void  DecryptMpqBlockSerpent(void * pvDataBlock, uint32_t dwLength, serpentSchedule_t * keySchedule);

uint32_t DetectFileKeyBySectorSize(uint32_t * EncryptedData, uint32_t dwSectorSize, uint32_t dwSectorOffsLen);
uint32_t DetectFileKeyByContent(void * pvEncryptedData, uint32_t dwSectorSize, uint32_t dwFileSize);
uint32_t DecryptFileKey(const char * szFileName, uint64_t MpqPos, uint32_t dwFileSize, uint32_t dwFlags);

int IsValidMD5(unsigned char * pbMd5);
int IsValidSignature(unsigned char * pbSignature, size_t cbSignatureSize);
int VerifyDataBlockHash(void * pvDataBlock, uint32_t cbDataBlock, unsigned char * expected_md5);
void CalculateDataBlockHash(void * pvDataBlock, uint32_t cbDataBlock, unsigned char * md5_hash);

/*-----------------------------------------------------------------------------
 * Handle validation functions
 */

TMPQArchive * IsValidMpqHandle(void * hMpq);
TMPQFile * IsValidFileHandle(void * hFile);

/*-----------------------------------------------------------------------------
 * Support for MPQ file tables
 */

uint64_t FileOffsetFromMpqOffset(TMPQArchive * ha, uint64_t MpqOffset);
uint64_t CalculateRawSectorOffset(TMPQFile * hf, uint32_t dwSectorOffset);

int ConvertMpqHeaderToFormat4(TMPQArchive * ha, uint64_t MpqOffset, uint64_t FileSize, uint32_t dwFlags);

TMPQHash * FindFreeHashEntry(TMPQArchive * ha, uint32_t dwStartIndex, uint32_t dwName1, uint32_t dwName2, uint32_t lcLocale);
TMPQHash * GetFirstHashEntry(TMPQArchive * ha, const char * szFileName);
TMPQHash * GetNextHashEntry(TMPQArchive * ha, TMPQHash * pFirstHash, TMPQHash * pPrevHash);
TMPQHash * AllocateHashEntry(TMPQArchive * ha, TFileEntry * pFileEntry, uint32_t lcLocale);

TMPQExtHeader * LoadExtTable(TMPQArchive * ha, uint64_t ByteOffset, size_t Size, uint32_t dwSignature, uint32_t dwKey);
TMPQHetTable * LoadHetTable(TMPQArchive * ha);
TMPQBetTable * LoadBetTable(TMPQArchive * ha);

TMPQBlock * LoadBlockTable(TMPQArchive * ha);
TMPQBlock * TranslateBlockTable(TMPQArchive * ha, uint64_t * pcbTableSize, int * pbNeedHiBlockTable);

uint64_t FindFreeMpqSpace(TMPQArchive * ha);

/* Functions that load the HET and BET tables */
int  CreateHashTable(TMPQArchive * ha, uint32_t dwHashTableSize);
int  LoadAnyHashTable(TMPQArchive * ha);
int  BuildFileTable(TMPQArchive * ha);
int  DefragmentFileTable(TMPQArchive * ha);

int  CreateFileTable(TMPQArchive * ha, uint32_t dwFileTableSize);
int  RebuildHetTable(TMPQArchive * ha);
int  RebuildFileTable(TMPQArchive * ha, uint32_t dwNewHashTableSize);
int  SaveMPQTables(TMPQArchive * ha);

TMPQHetTable * CreateHetTable(uint32_t dwEntryCount, uint32_t dwTotalCount, uint32_t dwHashBitSize, unsigned char * pbSrcData);
void FreeHetTable(TMPQHetTable * pHetTable);

TMPQBetTable * CreateBetTable(uint32_t dwMaxFileCount);
void FreeBetTable(TMPQBetTable * pBetTable);

/* Functions for finding files in the file table */
TFileEntry * GetFileEntryLocale2(TMPQArchive * ha, const char * szFileName, uint32_t lcLocale, uint32_t * PtrHashIndex);
TFileEntry * GetFileEntryLocale(TMPQArchive * ha, const char * szFileName, uint32_t lcLocale);
TFileEntry * GetFileEntryExact(TMPQArchive * ha, const char * szFileName, uint32_t lcLocale, uint32_t * PtrHashIndex);

/* Allocates file name in the file entry */
void AllocateFileName(TMPQArchive * ha, TFileEntry * pFileEntry, const char * szFileName);

/* Allocates new file entry in the MPQ tables. Reuses existing, if possible */
TFileEntry * AllocateFileEntry(TMPQArchive * ha, const char * szFileName, uint32_t lcLocale, uint32_t * PtrHashIndex);
int  RenameFileEntry(TMPQArchive * ha, TMPQFile * hf, const char * szNewFileName);
int  DeleteFileEntry(TMPQArchive * ha, TMPQFile * hf);

/* Invalidates entries for (listfile) and (attributes) */
void InvalidateInternalFiles(TMPQArchive * ha);

/* Retrieves information about the strong signature */
int QueryMpqSignatureInfo(TMPQArchive * ha, PMPQ_SIGNATURE_INFO pSignatureInfo);

/*-----------------------------------------------------------------------------
 * Support for alternate file formats (SBaseSubTypes.cpp)
 */

int ConvertSqpHeaderToFormat4(TMPQArchive * ha, uint64_t FileSize, uint32_t dwFlags);
TMPQHash * LoadSqpHashTable(TMPQArchive * ha);
TMPQBlock * LoadSqpBlockTable(TMPQArchive * ha);

int ConvertMpkHeaderToFormat4(TMPQArchive * ha, uint64_t FileSize, uint32_t dwFlags);
void DecryptMpkTable(void * pvMpkTable, size_t cbSize);
TMPQHash * LoadMpkHashTable(TMPQArchive * ha);
TMPQBlock * LoadMpkBlockTable(TMPQArchive * ha);
int SCompDecompressMpk(void * pvOutBuffer, int * pcbOutBuffer, void * pvInBuffer, int cbInBuffer);

/*-----------------------------------------------------------------------------
 * Common functions - MPQ File
 */

TMPQFile * CreateFileHandle(TMPQArchive * ha, TFileEntry * pFileEntry);
void * LoadMpqTable(TMPQArchive * ha, uint64_t ByteOffset, uint32_t dwCompressedSize, uint32_t dwTableSize, uint32_t dwKey, int * pbTableIsCut);
int  AllocateSectorBuffer(TMPQFile * hf);
int  AllocatePatchInfo(TMPQFile * hf, int bLoadFromFile);
int  AllocateSectorOffsets(TMPQFile * hf, int bLoadFromFile);
int  AllocateSectorChecksums(TMPQFile * hf, int bLoadFromFile);
int  WritePatchInfo(TMPQFile * hf);
int  WriteSectorOffsets(TMPQFile * hf);
int  WriteSectorChecksums(TMPQFile * hf);
int  WriteMemDataMD5(TFileStream * pStream, uint64_t RawDataOffs, void * pvRawData, uint32_t dwRawDataSize, uint32_t dwChunkSize, uint32_t * pcbTotalSize);
int  WriteMpqDataMD5(TFileStream * pStream, uint64_t RawDataOffs, uint32_t dwRawDataSize, uint32_t dwChunkSize);
void FreeFileHandle(TMPQFile ** hf);
void FreeArchiveHandle(TMPQArchive ** ha);

/*-----------------------------------------------------------------------------
 * Patch functions
 */

/* Structure used for the patching process */
typedef struct _TMPQPatcher
{
    uint8_t this_md5[MD5_DIGEST_SIZE];             /* MD5 of the current file state */
    uint8_t * pbFileData1;                         /* Primary working buffer */
    uint8_t * pbFileData2;                         /* Secondary working buffer */
    uint32_t cbMaxFileData;                        /* Maximum allowed size of the patch data */
    uint32_t cbFileData;                           /* Current size of the result data */
    uint32_t nCounter;                             /* Counter of the patch process */

} TMPQPatcher;

int IsIncrementalPatchFile(const void * pvData, uint32_t cbData, uint32_t * pdwPatchedFileSize);
int Patch_InitPatcher(TMPQPatcher * pPatcher, TMPQFile * hf);
int Patch_Process(TMPQPatcher * pPatcher, TMPQFile * hf);
void Patch_Finalize(TMPQPatcher * pPatcher);

/*-----------------------------------------------------------------------------
 * Utility functions
 */

int CheckWildCard(const char * szString, const char * szWildCard);
int IsInternalMpqFileName(const char * szFileName);

const char * GetPlainFileName(const char * szFileName);
const char * GetPlainFileName(const char * szFileName);

void CopyFileName(char * szTarget, const char * szSource, size_t cchLength);
void CopyFileName(char * szTarget, const char * szSource, size_t cchLength);

/*-----------------------------------------------------------------------------
 * Internal support for MPQ modifications
 */

int SFileAddFile_Init(
    TMPQArchive * ha,
    const char * szArchivedName,
    uint64_t ft,
    uint32_t dwFileSize,
    uint32_t lcLocale,
    uint32_t dwFlags,
    TMPQFile ** phf
    );

int SFileAddFile_Write(
    TMPQFile * hf,
    const void * pvData,
    uint32_t dwSize,
    uint32_t dwCompression
    );

int SFileAddFile_Finish(
    TMPQFile * hf
    );

/*-----------------------------------------------------------------------------
 * Attributes support
 */

int  SAttrLoadAttributes(TMPQArchive * ha);
int  SAttrFileSaveToMpq(TMPQArchive * ha);

/*-----------------------------------------------------------------------------
 * Listfile functions
 */

int  SListFileSaveToMpq(TMPQArchive * ha);

/*-----------------------------------------------------------------------------
 * Weak signature support
 */

int SSignFileCreate(TMPQArchive * ha);
int SSignFileFinish(TMPQArchive * ha);

/*-----------------------------------------------------------------------------
 * Dump data support
 */

#ifdef _STORMLIB_DUMP_DATA

void DumpMpqHeader(TMPQHeader * pHeader);
void DumpHashTable(TMPQHash * pHashTable, DWORD dwHashTableSize);
void DumpHetAndBetTable(TMPQHetTable * pHetTable, TMPQBetTable * pBetTable);

#else

#define DumpMpqHeader(h)           /* */
#define DumpHashTable(h, s)        /* */
#define DumpHetAndBetTable(h, b)   /* */

#endif

#endif /* _STORMCOMMON_H */

